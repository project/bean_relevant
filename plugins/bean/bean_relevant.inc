
<?php
/**
 * @file
 * Bean: Relevant Content bean plugin.
 */

class RelevantContentBean extends BeanPlugin {
  /**
   * Declares default block settings.
   */
  public function values() {
    return array(
      'display_options' => array(),
      'filter_options' => array(),
    );
  }

  /**
   * Builds extra settings for the block edit form.
   */
  public function form($bean, $form, &$form_state) {
    $form = array();

    $form['header'] = array(
      '#type' => 'fieldset',
      '#tree' => FALSE,
      '#title' => t('Display Relevant Content'),
    );

    $form['header']['display_options'] = array(
      '#type' => 'fieldset',
      '#tree' => TRUE,
      '#title' => t('Display Options'),
    );

    $form['header']['display_options']['num_posts'] = array(
      '#type' => 'select',
      '#title' => t('Number of nodes'),
      '#description' => t('Maximum number of nodes.'),
      '#options' => drupal_map_assoc(range(1, 20)),
      '#default_value' => isset($bean->display_options['num_posts']) ?
        $bean->display_options['num_posts'] : 3,
    );

    $form['header']['display_options']['node_display'] = array(
      '#type' => 'select',
      '#title' => t('Node display'),
      '#description' => t('Display listed nodes in this format.'),
      '#options' => array(
        'title' => 'Linked Title',
        'default' => 'Default',
        'teaser' => 'Teaser'),
      '#default_value' => isset($bean->display_options['node_display']) ?
        $bean->display_options['node_display'] : 'title',
    );

    $form['header']['display_options']['order_on'] = array(
      '#type' => 'select',
      '#title' => t('Order on'),
      '#description' => t('Display listed nodes ordered on this property.'),
      '#options' => array(
        'created' => 'Created',
        'changed' => 'Updated'),
      '#default_value' => isset($bean->display_options['node_display']) ?
        $bean->display_options['order_on'] : 'default',
    );


    $form['header']['display_options']['sticky'] = array(
      '#type' => 'checkbox',
      '#title' => t('Order by \'Sticky\'.'),
      '#options' => array(
        0 => 'No',
        1 => 'Yes'),
      '#default_value' => isset($bean->display_options['sticky']) ?
        $bean->display_options['sticky'] : 0,
    );

    $form['header']['display_options']['order'] = array(
      '#type' => 'select',
      '#title' => t('Node order'),
      '#description' => t('Display listed nodes in this order.'),
      '#options' => array(
        'ASC' => 'Ascending',
        'DESC' => 'Descending'),
      '#default_value' => isset($bean->display_options['order']) ?
        $bean->display_options['order'] : 'default',
    );

    $form['header']['filter_options'] = array(
      '#type' => 'fieldset',
      '#tree' => TRUE,
      '#title' => t('Filter Options'),
    );

    $form['header']['filter_options']['content'] = array(
      '#type' => 'fieldset',
      '#tree' => TRUE,
      '#title' => t('Content'),
    );

    $form['header']['filter_options']['content']['use_nodes'] = array(
      '#type' => 'select',
      '#title' => t('Filter on content type?'),
      '#options' => array(
        0 => 'No',
        1 => 'Yes'),
      '#default_value' => isset($bean->filter_options['content']['use_nodes']) ?
        $bean->filter_options['content']['use_nodes'] : 0,
    );

    $form['header']['filter_options']['content']['current_node'] = array(
      '#type' => 'select',
      '#title' => t('Filter on same content type as current node?'),
      '#options' => array(
        1 => 'Yes',
        0 => 'No, select content types'),
      '#default_value' => isset($bean->filter_options['content']['current_node']) ?
        $bean->filter_options['content']['current_node'] : 1,
      '#states' => array(
        'visible' => array(
          ':input[name="filter_options[content][use_nodes]"]' => array('value' => 1),
        ),
      ),
    );

    //Get Node types
    foreach (node_type_get_types() as $node_type) {
      $node_types[$node_type->type] = $node_type->name;
    }

    $form['header']['filter_options']['content']['content_types'] = array(
      '#type' => 'select',
      '#title' => t('Which content types?'),
      '#options' => $node_types,
      '#multiple' => TRUE,
      '#default_value' => isset($bean->filter_options['content']['content_types']) ?
              $bean->filter_options['content']['content_types'] : 0,
      '#states' => array(
        'visible' => array(
          ':input[name="filter_options[content][current_node]"]' => array('value' => 0),
          ':input[name="filter_options[content][use_nodes]"]' => array('value' => 1),
        ),
      ),
    );

    $form['header']['filter_options']['taxonomy'] = array(
      '#type' => 'fieldset',
      '#tree' => TRUE,
      '#title' => t('Taxonomy'),
    );


    $form['header']['filter_options']['taxonomy']['use_vocab'] = array(
      '#type' => 'select',
      '#title' => t('Filter on taxonomy?'),
      '#options' => array(
        0 => 'No',
        1 => 'Yes'),
      '#default_value' => isset($bean->filter_options['taxonomy']['use_vocab']) ?
        $bean->filter_options['taxonomy']['use_vocab'] : 0,
    );

    $term_depth_options = array_merge(array('' => 'Unlimited'), drupal_map_assoc(range(0, 10)));
    $form['header']['filter_options']['taxonomy']['term_depth'] = array(
      '#type' => 'select',
      '#title' => t('Term depth'),
      '#description' => t("Select the depth of child terms to be used for filtering.
        Select '0' to only return nodes tagged with these specific terms."),
      '#options' => $term_depth_options,
      '#states' => array(
        'visible' => array(
          ':input[name="filter_options[taxonomy][use_vocab]"]' => array('value' => 1),
        ),
      ),
    );


    $form['header']['filter_options']['taxonomy']['contextual'] = array(
      '#type' => 'select',
      '#title' => t('Filter by taxonomy terms contextually?'),
      '#options' => array(
        0 => 'No',
        1 => 'Yes'),
      '#default_value' => isset($bean->filter_options['taxonomy']['contextual']) ?
        $bean->filter_options['taxonomy']['contextual'] : 0,
      '#states' => array(
        'visible' => array(
          ':input[name="filter_options[taxonomy][use_vocab]"]' => array('value' => 1),
        ),
      ),
    );


    $form['header']['filter_options']['taxonomy']['context_vocab'] = array(
      '#type' => 'fieldset',
      '#tree' => TRUE,
      '#title' => t('Vocabularies'),
      '#description' => t('Choose vocabularies to filter on, leave blank for all.'),
      '#states' => array(
        'visible' => array(
          ':input[name="filter_options[taxonomy][contextual]"]' => array('value' => 1),
          ':input[name="filter_options[taxonomy][use_vocab]"]' => array('value' => 1),
        ),
      ),
    );

    foreach (taxonomy_get_vocabularies() as $vocab) {
      $form['header']['filter_options']['taxonomy']['context_vocab'][$vocab->vid] = array(
        '#type' => 'checkbox',
        '#title' => $vocab->name,
        '#options' => array(0 => t('Closed'), 1 => t('Active')),
        '#default_value' => (isset($bean->filter_options['taxonomy']['context_vocab'][$vocab->vid])
          && $bean->filter_options['taxonomy']['context_vocab'][$vocab->vid] == 1) ?
          $bean->filter_options['taxonomy']['context_vocab'][$vocab->vid] : 0,
      );
    }

    $form['header']['filter_options']['taxonomy']['vocab'] = array(
      '#type' => 'fieldset',
      '#tree' => TRUE,
      '#title' => t('Vocabularies'),
      '#states' => array(
        'visible' => array(
          ':input[name="filter_options[taxonomy][contextual]"]' => array('value' => 0),
          ':input[name="filter_options[taxonomy][use_vocab]"]' => array('value' => 1),
        ),
      ),
    );

    foreach (taxonomy_get_vocabularies() as $vocab) {

      $form['header']['filter_options']['taxonomy']['vocab'][$vocab->vid] = array(
        '#type' => 'checkbox',
        '#title' => $vocab->name,
        '#options' => array(0 => t('Closed'), 1 => t('Active')),
        '#default_value' => (isset($bean->filter_options['taxonomy']['vocab'][$vocab->vid])
          && $bean->filter_options['taxonomy']['vocab'][$vocab->vid] == 1) ?
          $bean->filter_options['taxonomy']['vocab'][$vocab->vid] : 0,
        '#ajax' => array(
          'callback' => 'bean_relevant_terms_callback',
          'wrapper' => 'bean_relevant_' . $vocab->vid . '_terms_container',
          'method' => 'replace',
          'effect' => 'fade',
          'progress' => array(
            'type' => 'throbber',
            'message' => t('Getting Terms.')
          )
        )
      );
      $form['header']['filter_options']['taxonomy']['vocab'][$vocab->vid . 'terms'] = array(
        '#prefix' => '<div id="bean_relevant_' . $vocab->vid . '_terms_container">',
        '#suffix' => '</div>'
      );

      if (isset($bean->filter_options['taxonomy']['vocab'][$vocab->vid]) &&
        $bean->filter_options['taxonomy']['vocab'][$vocab->vid] == 1) {
        $display = TRUE;
        if (isset($form_state['values']['filter_options']['taxonomy']['vocab'][$vocab->vid])) {
          if ($form_state['values']['filter_options']['taxonomy']['vocab'][$vocab->vid] == 1) {
            $display = TRUE;
          }
          else {
            $display = FALSE;
          }
        }
      }
      else {
        if (isset($form_state['values']['filter_options']['taxonomy']['vocab'][$vocab->vid])) {
          if ($form_state['values']['filter_options']['taxonomy']['vocab'][$vocab->vid] == 1) {
            $display = TRUE;
          }
          else {
            $display = FALSE;
          }
        }
        else {
          $display = FALSE;
        }
      }

      if ($display) {
        $form['header']['filter_options']['taxonomy']['vocab'][$vocab->vid . 'terms'] = array(
          '#type' => 'fieldset',
          '#title' => t('Select Terms'),
          '#collapsible' => FALSE,
          '#id' => 'bean_relevant_' . $vocab->vid . '_terms_container'
        );


        $term_options = array();
        $term_tree = taxonomy_get_tree($vocab->vid, 0);
        foreach ($term_tree as $key => $term) {
          $term_options[$term->tid] = str_repeat('-', $term->depth) . $term->name;
        }

        $form['header']['filter_options']['taxonomy']['vocab'][$vocab->vid . 'terms']['something'] = array(
          '#type' => 'select',
          '#title' => t($vocab->name . ' Taxonomy'),
          '#options' => $term_options,
                '#multiple' => TRUE,
          '#default_value' => isset($bean->filter_options['taxonomy']['vocab'][$vocab->vid . 'terms']['something'])
            ? $bean->filter_options['taxonomy']['vocab'][$vocab->vid . 'terms']['something'] : '',
        );
      }
    }
    return $form;
  }

  /**
   * Displays the bean.
   */
  public function view($bean, $content, $view_mode = 'default', $langcode = NULL) {
    $data = new EntityFieldQuery;

    $result = $data
      ->entityCondition('entity_type', 'node')
      ->range(0, $bean->display_options['num_posts']);

    if ($bean->display_options['sticky'] == 1) {
      $result->propertyOrderBy('sticky', 'DESC');
    }

    $result->propertyOrderBy($bean->display_options['order_on'], $bean->display_options['order']);

    if ($bean->filter_options['content']['use_nodes'] == 1) {
      switch ($bean->filter_options['content']['current_node']) {
        case 1:
          $node = menu_get_object();
          $bundles = ($node) ? array($node->type) : array(0);
          break;

        case 0:
          $bundles = array_keys($bean->filter_options['content']['content_types']);
          break;
      }

      $result->entityCondition('bundle', $bundles, 'IN');
    }

    if ($bean->filter_options['taxonomy']['use_vocab'] == 1) {
      $term_nodes = array();
      $node = menu_get_object();

      switch ($bean->filter_options['taxonomy']['contextual']) {
        case 1:
          $vid_options = $bean->filter_options['taxonomy']['context_vocab'];

          //get Vocabulary ID's to use
          $vids = array();
          foreach ($vid_options as $vid => $vid_use) {
            if ($vid_use == 1) {
              $vids[$vid] = $vid;
            }
          }

          //get Taxonomy fields
          $taxonomy_fields = array();
          foreach (field_info_fields() as $field => $field_info) {
           if ($field_info['type'] == 'taxonomy_term_reference') {
             $taxonomy_fields[$field_info['id']] = $field_info['field_name'];
           }
          }

          if ($node) {
            foreach ($taxonomy_fields as $tax_field) {
              $field = $node->$tax_field;
              foreach ($field[LANGUAGE_NONE] as $term_info) {
                $term = taxonomy_term_load($term_info['tid']);
                $tids = array($term_info['tid']);
                $depth = ($bean->filter_options['taxonomy']['term_depth'] == '') ? NULL
                  : $bean->filter_options['taxonomy']['term_depth'];
                foreach (taxonomy_get_tree($term->vid, $term->tid, $depth) as $tree_term) {
                  $tids[] = $tree_term->tid;
                }
                foreach ($tids as $tid) {
                  foreach (taxonomy_select_nodes($tid, FALSE) as $nid) {
                    if($nid != $node->nid) {
                      if (!empty($vids)) {
                        if (isset($term_info['taxonomy_term']) == 1) {
                          if (array_search($term_info['taxonomy_term']->vid,$vids) != ''){
                            $term_nodes[$nid] = $nid;
                          }
                        }
                      }
                      else {
                        $term_nodes[$nid] = $nid;
                      }
                    }
                  }
                }
              }
            }
          }

          break;

        case 0:
          foreach ($bean->filter_options['taxonomy']['vocab'] as $vocab_key => $vocab) {
            if (is_numeric($vocab_key) and $vocab == 1) {
              foreach ($bean->filter_options['taxonomy']['vocab'][$vocab_key . 'terms']['something'] as $tid_selected) {
                $term = taxonomy_term_load($tid_selected);
                $tids = array($tid_selected);
                $depth = ($bean->filter_options['taxonomy']['term_depth'] == '') ? NULL
                  : $bean->filter_options['taxonomy']['term_depth'];
                foreach (taxonomy_get_tree($term->vid, $term->tid, $depth) as $tree_term) {
                  $tids[] = $tree_term->tid;
                }
                foreach ($tids as $tid) {
                  foreach (taxonomy_select_nodes($tid, FALSE) as $nid) {
                    if($node) {
                      if ($nid != $node->nid) {
                        $term_nodes[$nid] = $nid;
                      }
                    }
                    else {
                      $term_nodes[$nid] = $nid;
                    }
                  }
                }
              }
            }
          }
          break;
      }
      if (!empty($term_nodes)) {
        $result->propertyCondition('nid', $term_nodes, 'IN');
      }
      else {
        $result->propertyCondition('nid', 0);
      }
    }

    $result = $data->execute();

    if (isset($result['node'])) {

      $nodes = entity_load('node', array_keys($result['node']));
      $items = array();
      foreach ($nodes as $node) {

        $items[] = l($node->title, 'node/' . $node->nid);

        if ($bean->display_options['node_display'] != 'title') {
          $display_content = node_view($node, $bean->display_options['node_display']);
          $body = $display_content['body'][0]['#markup'];
          $items[] = $body;
        }
      }

      $output = '';
      foreach ($items as $item) {
        $output .= '<div>' . $item . '</div>';
      }

    }
    else {
      $output = '';
      $bean->title = '';
    }

    $content['bean_relevant']['#markup'] = $output;
    return $content;
  }

}

/*
 * Relevant terms callback.
 */
function bean_relevant_terms_callback($form, $form_state) {
  $vocab = taxonomy_vocabulary_machine_name_load($form_state['triggering_element']['#title']);
  $term = $vocab->vid;

  return $form['header']['filter_options']['taxonomy']['vocab'][$term . 'terms'];
}

